bname=$(basename "$0")
bname=${bname/.bash/}"_$CI_BUILD_REF_NAME"
##########
export LM_LICENSE_FILE=/opt/pgi/license.dat
source /opt/pgi/linux86-64/15.7/pgi.sh
export LD_LIBRARY_PATH=/opt/pgi/15.7/share_objects/lib64:$LD_LIBRARY_PATH
#
wrk=$1
#
if [ ! -d $bname ]   
then
   ./setup --fc=pgf90 --omp --type=debug --cmake-options="-DENABLE_TENSOR_CLASS=ON -DBUILDNAME=$bname" $bname
fi
#
cd $bname
#
#ctest -D Nightly --track GitLabCI -L ContinuousIntegration
ctest -D Nightly --track GitLabCI
exit $?
