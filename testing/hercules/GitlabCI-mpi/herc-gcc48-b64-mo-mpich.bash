bname=$(basename "$0")
bname=${bname/.bash/}"_$CI_BUILD_REF_NAME"
##########
source /opt/mpich-3.1.4-gnu-4.8/source.bash
#
wrk=$1
# !Try to compile the documentation
cd doc
make
suc=$?
if [ $suc != 0 ]
then
   return $suc
else
   cd ..
fi
#
if [ ! -d $bname ]   
then
   ./setup --fc=mpif90 --omp --mpi --type=debug --cmake-options="-DENABLE_TENSOR_CLASS=ON -DBUILDNAME=$bname" $bname
fi
#
cd $bname
#
#ctest -D Nightly --track GitLabCI -L ContinuousIntegration
ctest -D Nightly --track GitLabCI
exit $?
