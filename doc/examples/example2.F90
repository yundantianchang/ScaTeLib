!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Author:   Patrick Ettenhuber (pettenhuber@gmail.com)
! Date:     February, 2016
! File:     This file contains a simple example of how to use tensor IO 
! License : This file is subject to all the licensing conditions under which
!           ScaTeLib is distributed
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

! Compile: where ../../build has to be replaced by your specific build path and
! the arguments in parenthesis are only necessary if you compiled ScaTeLib with
! these, conversely this means that you need to provide every lib you used for
! compiling ScaTeLib when linking this example:

! Compile: gfortran -I../../build/modules  -c example2.F90 -o example2.F90.o
! Link:    gfortran ../../build/src/CMakeFiles/ScaTeLib.dir/tensor_interface.F90.o example2.F90.o -o example2.x  ../../build/src/libScaTeLib.a ( -framework Accelerate -fopenmp )

program simple_tensor_use
   use tensor_interface_module
   implicit none
   type(tensor)    :: A,B,C
   integer(kind=8) :: bytes_used
   integer :: io_err


   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Tensor operations will not work here !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   !Initialize the tensor intrerface
   call tensor_initialize_interface(tensor_comm_null, mem_ctr = bytes_used)


   !please feel free to test any tensor operation here!
   call tensor_init(A,[140,144,124,180])

   ! Give the tensor a label, all (only) alphanumerical characters bcome the
   ! file name with the extension .tns
   call tensor_set_label(A,"This becåmes my file name")

   ! set some data
   call tensor_random(A)

   call print_norm(A,"This is my random norm:",print_=.true.)

   ! write the file
   io_err = tensor_write(A)

   call tensor_free(A)

   ! Now initialize tensor B with the same dimensions and read the file
   call tensor_init(B,[140,144,124,180])

   ! Give the tensor a label, all alphanumerical characters
   io_err = tensor_read(B,fname="Thisbecmesmyfilename.tns")

   ! Check the label
   print *, "The original tensor was labeled:",tensor_get_label(B)
   call print_norm(B,"This and had the norm: ",print_=.true.)

   ! The file is not automatically deleted when a tensor is freed (for obvious
   ! reasons)
   io_err = tensor_delete_file(B)

   !Free B
   call tensor_free(B)


   ! Finalize the tensor interface
   call tensor_finalize_interface

   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Tensor operations will not work here !
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

end program simple_tensor_use
