#/usr/bin/python
# The Order of the Code and Documentation (OCD) checker script
#
#     .-'''-.           _..._                  
#    '   _    \      .-'_..._''. _______       
#  /   /` '.   \   .' .'      '.\\  ___ `'.    
# .   |     \  '  / .'            ' |--.\  \   
# |   '      |  '. '              | |    \  '  
# \    \     / / | |              | |     |  ' 
#  `.   ` ..' /  | |              | |     |  | 
#     '-...-'`   . '              | |     ' .' 
#                 \ '.          . | |___.' /'  
#                  '. `._____.-'//_______.'/   
#                    `-.______ / \_______|/    
# 
# This ascii art has been generated with: http://patorjk.com/software/taag/#p=display&f=Crazy&t=OCD
#
#
# The purpose of this script is solely to update the documentation of the ScaTeLib doc in doc.tex
# It is distributed with and under the same licencse agreement as the rest of ScaTeLib
# author: Patrick Ettenhuber (pettenhuber@gmail.com)
# date: January, 2016

# Import stuff
import os
from shutil import copyfile, move

def update_old_tensor_type(ofile,slins,ilin,rpath):
   j     = ilin + 1
   line  = slins[ilin]
   Found = False

   pattern1 = "firstline="
   pattern2 = "lastline="

   while not Found:

      line = slins[j]
      Found = pattern1 in line and pattern2 in line

      if( not Found ):
         ofile.write(line)
         j+=1

   fl_b = line.find(pattern1) + len(pattern1)
   fl_e = line.find(",",fl_b)
   if fl_e == -1 :
      fl_e = line.find("]",fl_b)
   ll_b = line.find(pattern2) + len(pattern2)
   ll_e = line.find(",",ll_b)
   if ll_e == -1 :
      ll_e = line.find("]",ll_b)

   if ll_b < fl_b:
      print("My OCD is not pleased: please make sure that lastline= occurs after firstline= in line "+str(j))

   # search the tensor_type_def for the correct line numbers
   ttf = open(rpath+"../src/tensor_type_def.F90",'r')
   f_line = 0
   t_line = 0
   for tt_iline, tt_line in enumerate(ttf):
      if "end type tensor" == tt_line.strip():
         t_line = tt_iline
      elif "type :: tensor" == tt_line.strip():
         f_line = tt_iline

   # build the new line with the updated numbers
   line = line[:fl_b] + str(f_line+1) + line[fl_e:ll_b] + str(t_line+1) + line[ll_e:]

   #write the line
   ofile.write(line)

   # return the new counter
   return j 

def update_subfuncint(ofile,slins,ilin,nlist,rpath):
   line  = slins[ilin]
   Found = False
   sub_n = nlist[0] #subroutine/function/interface name
   doc_b = nlist[1]-1 #line where the documentation starts
   sub_d = nlist[2]-1 #line where the subroutine is defined
   doc_e = nlist[3]-1 #line where the documentation ends
   sub_e = nlist[4]-1 #line where the subroutine ends
   fpath = nlist[5] #path to the file without 

   #check whether the documentation has been done properly so that it can be displayed in the doc
   f = open(rpath+"../"+fpath,'r')
   fl = f.readlines()
   f.close()

   # checking and collecting variables
   AuthorFound = False      #check for the prsence of the Author: field
   DateFound   = False      #check for the prsence of the Date: field
   IntentFound = False      #check for the prsence of the Intent: field
   ReadInput   = False      #flag to check whether documented input is currently read
   ReadOutput  = False      #flag to check whether documented Output is currently read
   Input_args_doc  = []     #Documented input variables
   Output_args_doc = []     #Documented Output variables
   Argument_list   = []     #Argument list 
   Input_args_imp  = []     #Implemented input variables
   Output_args_imp = []     #Implemented Output variables (inout is listed under output)
   for il, l in enumerate(fl):
      
      # stop the loop after the doc has been checked
      if il > doc_e:
         break

      # get the argument list
      if il == sub_d:
         als_b = l.find("(")
         if als_b != -1 :
            als_str = l[als_b+1:].strip()
            if "&" in l:
               als_str = als_str.replace("&","")
               jl = il + 1
               while fl[jl].strip().startswith("&"):
                  als_str += fl[jl].strip().replace("&","")
                  jl      += 1
            als_str = als_str.replace(")","").strip()
            if "result(" in als_str:
               als_str = als_str.replace("result(",",").strip()
            als_lst = als_str.split(",")
            for als in als_lst:
               Argument_list.append(als.strip())

      if il >= doc_b and il <= doc_e:
         if l.strip().startswith("!Author:"):
            AuthorFound = True
         if l.strip().startswith("!Date:"):
            DateFound = True
         if l.strip().startswith("!Intent:"):
            IntentFound = True
         if len(l.strip()) > 70:
            print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(il+1))
            print("The line is too long to be documented")
            exit(-1)

         if l.strip().startswith("!") and "Input" in l:
            if l.strip().startswith("!Input:"):
               ReadInput  = True
               ReadOutput = False
               input_b    = il
            else:
               print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(il+1))
               print("To be read again. The functional comment is \"!Input:\"")
               exit(-1)
         if l.strip().startswith("!") and "Output" in l:
            if l.strip().startswith("!Output:"):
               ReadOutput = True
               ReadInput  = False
               output_b   = il
            else:
               print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(il+1))
               print("To be read again. The functional comment is \"!Output:\"")
               exit(-1)

         if il >= sub_d :
            ReadOutput = False
            ReadInput  = False

         if (ReadInput or ReadOutput)  and not (AuthorFound and DateFound and IntentFound):
            print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(il+1))
            print("Author, Date and Intent need to be defined before Input/Output")
            exit(-1)

         if ReadInput and il > input_b:
            arg_c = l.replace("!"," ").strip().startswith("-")
            if(arg_c):
               contin = False
            else:
               # The first l needs to contain the - thus beg_d must!! be defined here
               if (len(l) - len(l.replace("!"," ").lstrip())) != beg_d :
                  print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(il+1))
                  print("A continuation line needs to start at the same offset as the line after the argument")
                  exit(-1)
               else:
                  contin = True

            # if this is not a continuation l find the end of the argument and store it
            if not contin :
               arg_b = l.find("-")
               arg_e = l.find(":",arg_b)
               if(arg_c and arg_e == -1):
                  print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(il+1))
                  print("an input argument ends with :")
                  exit(-1)
               beg_d = arg_e + 1 + len(l[arg_e+1:]) - len(l[arg_e+1:].lstrip()) 

               Input_args_doc.append(l[arg_b+1:arg_e].strip())
            

         if ReadOutput and il > output_b:

            arg_c = l.replace("!"," ").strip().startswith("-")
            if(arg_c):
               contin = False
            else:
               # The first line needs to contain the - thus beg_d must!! be defined here
               if (len(l) - len(l.replace("!"," ").lstrip())) != beg_d :
                  print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(il+1))
                  print("A continuation line needs to start at the same offset as the line after the argument")
                  exit(-1)
               else:
                  contin = True

            # if this is not a continuation line find the end of the argument and store it
            if not contin :
               arg_b = l.find("-")
               arg_e = l.find(":",arg_b)
               if(arg_c and arg_e == -1):
                  print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(il+1))
                  print("an output argument ends with :")
                  exit(-1)
               beg_d = arg_e + 1 + len(l[arg_e+1:]) - len(l[arg_e+1:].lstrip()) 

               Output_args_doc.append(l[arg_b+1:arg_e].strip())


      if il >= doc_b and il < sub_d:
         if l.find("!") != 2:
            print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(il+1))
            print("We use three spaces before \"!\" for the documentation")
            exit(-1)

      if il > sub_d and il <= doc_e and not l.strip().startswith("&"):
         if l.strip().startswith("!"):
            print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(il+1))
            print("No comments allowed in the data declaration, this should be done in the header")
            exit(-1)
         #skip implicit none line and read the variables
         if not "implicit none" in l.lower() :
            dd_b = l.find("::") + 2
            if( dd_b == -1):
               print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(il+1))
               print("Variable definitions need a ::")
               exit(-1)

            # Remove parenthesis from the variable declarations
            pb = l.find("(",dd_b)
            ml = l[:pb] 
            while pb != -1:
               cb = 1
               eb = pb
               while cb != 0:
                  eb += 1
                  if( eb > len(l)):
                     print("Did you forget to close a parenthesis in ../"+fpath+" +"+str(il+1)+"?")
                     exit(-1)
                  if(l[eb] == ")" ):
                     cb += -1
                  if(l[eb] == "(" ):
                     cb += 1
               nb  = l.find("(",eb+1)
               ml += l[eb+1:nb]
               pb  = nb
            ml += l[-1]
            if "intent(in)" in ml.lower():
               for var in ml[dd_b:ml.find("!")].split(","):
                  Input_args_imp.append(var.strip())
            #elif "intent(out)" in l.lower() or "intent(inout)" in l.lower():
            else:
               for var in ml[dd_b:ml.find("!")].split(","):
                  Output_args_imp.append(var.strip())


   #check if the required fields are present
   if not AuthorFound or not DateFound or not IntentFound:
      print("Documentation incomplete for "+sub_n+" in ../"+fpath+" +"+str(sub_d))
      if not AuthorFound:
         print("A documented Subroutine needs an \"!Author:\" field in the header")
      if not DateFound:
         print("A documented Subroutine needs an \"!Date:\" field in the header")
      if not IntentFound:
         print("A documented Subroutine needs an \"!Intent:\" field in the header")
      exit(-1)

   # check whether all variables in the arguement list have been documented (for non interface routines)
   if not fl[sub_d].strip().lower().startswith("interface"):
      for InVar in Input_args_doc:
         if InVar not in Argument_list:
            print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(sub_d))
            print("Input argument \""+InVar+"\" is documented but not used (case sensitive)")
            exit(-1)
         del Argument_list[Argument_list.index(InVar)]
         if InVar not in Input_args_imp:
            print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(sub_d))
            if InVar in Output_args_imp:
               print("Input argument \""+InVar+"\" should actually be an output argument")
            else:
               print(Input_args_imp)
               print("Input argument \""+InVar+"\" does not match any intent(in) variables (case sensitive)")
            exit(-1)
      for OutVar in Output_args_doc:
         if OutVar not in Argument_list:
            print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(sub_d))
            print("Output argument \""+OutVar+"\" is documented but not used (case sensitive)")
            exit(-1)
         del Argument_list[Argument_list.index(OutVar)]
         if OutVar not in Output_args_imp:
            print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(sub_d))
            if OutVar in Input_args_doc:
               print("Output argument \""+OutVar+"\" should actually be an input argument")
            else:
               print("Output argument \""+OutVar+"\" does not match any intent(inout)/intent(out) variables or variables without intent (case sensitive)")
            exit(-1)

   for arg in Argument_list:
      if len(arg.strip()) > 0 :
         print("Documentation incorrect for "+sub_n+" in ../"+fpath+" +"+str(sub_d))
         print("The following variable has not been documented:",arg.strip())
         exit(-1)
   del Argument_list


   #second argument is the description which needs to be extracted
   description_b = line.find("{",line.find("{")+1) + 1
   i = 0
   openpar = 1
   while openpar != 0:
      if line[description_b + i] == "}":
         openpar += -1
      elif line[description_b + i] == "{":
         openpar +=  1
      i += 1
   i = i - 1
   description_e = description_b + i
   description   = line[description_b:description_e]

   # build the new line with the updated numbers
   line = "\subfuncint{"+nlist[0]+"}{"+description+"}{"+str(nlist[1])+"}{"+str(nlist[3])+"}{"+nlist[5]+"}\n"

   #write the line
   ofile.write(line)

   # return the new counter
   return ilin

def update_interfacedto(ofile,slins,ilin,nlist,rpath):
   line  = slins[ilin]
   Found = False
   sub_n = nlist[0] #subroutine/function/interface name
   doc_b = nlist[1] #line where the documentation starts
   sub_d = nlist[2] #line where the subroutine is defined
   doc_e = nlist[3] #line where the documentation ends
   sub_e = nlist[4] #line where the subroutine ends
   fpath = nlist[5] #path to the file without ../

   # build the new line with the updated numbers
   line = "\interfacedto{"+nlist[0]+"}{generated}{"+str(nlist[1])+"}{"+str(nlist[3])+"}{"+nlist[5]+"}\n"

   #write the line
   ofile.write(line)

   # return the new counter
   return ilin


#The scripts location
rpath = os.path.dirname(os.path.realpath(__file__))+"/"
#Find things to document in ScaTeLib
precompiler_set = [] #Get all precompiler flags used in F90 files of ScaTeLib
cmake_opts_set  = [] #Get all Cmake Options used in ScaTeLib
paramco_set     = [] #Get all parameters and counters used in ScaTeLib
funcdoc_data    = [] #Get all documentation of subroutines, functions and interfaces
docfound        = True

#Only include files from the src dir for finding subroutines
whitelist       = [rpath+"../src"]
for root, dirs, files in os.walk(rpath+"../",topdown=True):

   # Modify the directories such that only paths in whitelist are followed (no build paths in <root>)
   dirs[:] = [ d for d in dirs for w in whitelist if os.path.join(root,d).startswith(w) ]

   for fi in files:            

      # gather info from fortran files
      if fi.endswith(".F90"):
         fpath = os.path.join(root, fi)
         f = open(fpath,'r')
         fl = f.readlines()
         f.close()

         for iline,line in enumerate(fl):
            uline = line.upper().strip()
            line  = line.strip()

            if uline.startswith("SUBROUTINE") or uline.startswith("PURE SUBROUTINE") or ((uline.startswith("FUNCTION") or uline.startswith("PURE") or uline.startswith("INTEGER") or uline.startswith("CHARACTER") or uline.startswith("REAL") or uline.startswith("LOGICAL")) and "FUNCTION" in uline) or uline.startswith("INTERFACE"):

               # check which type of function and extract the function name
               if uline.startswith("SUBROUTINE"):
                  b_name = uline.find("SUBROUTINE") + len("SUBROUTINE")
                  e_name = uline.find("(",b_name)
                  lab = "SUBROUTINE"
               elif uline.startswith("PURE SUBROUTINE"):
                  b_name = uline.find("PURE SUBROUTINE") + len("PURE SUBROUTINE")
                  e_name = uline.find("(",b_name)
                  lab = "SUBROUTINE"
               elif "PURE FUNCTION" in uline and "(" in uline :
                  b_name = uline.find("PURE FUNCTION") + len("PURE FUNCTION")
                  e_name = uline.find("(",b_name)
                  lab = "FUNCTION"
               elif "FUNCTION" in uline and "(" in uline :
                  b_name = uline.find("FUNCTION") + len("FUNCTION")
                  e_name = uline.find("(",b_name)
                  lab = "FUNCTION"
               elif uline.startswith("INTERFACE"):
                  b_name = uline.find("INTERFACE") + len("INTERFACE") 
                  b_name = b_name + len(uline[b_name]) - len(uline[b_name].strip())
                  e_name = uline.find(" ",b_name)
                  lab = "INTERFACE"

               # if the routine/interface has no arguments
               if e_name == -1 :
                  e_name = len(uline)

               # use the original case of the definition when storing the function
               func_name = line[ b_name:e_name ].strip()

               # there should be no & between the function name and the argument list !!
               if "&" in func_name:
                  print("My OCD is not pleased: uline breaks are not yet supported in the function definition in ",os.path.join(root,fi)," +"+str(iline)+"")
                  exit(-1)

               jline = iline - 1
               # Find the lines before the defininition of a subroutine which should be included in the doc, i.e. all commented lines
               while fl[jline].upper().strip().startswith("!"):
                  jline += -1
               jline += 1
               #check if there is documentation available
               if( jline == iline ):
                  print("WARNING: no documentation found for "+func_name+" in ",os.path.join(root.replace(rpath+"../",""),fi)," +"+str(iline))
                  docfound = False

               # Find the lines following the defininition of a subroutine which should be included in the doc, i.e. all lines beginning with &
               kline = iline 
               lline = iline + 1
               while not fl[lline].upper().strip().startswith("END "+lab+" "+func_name.upper()):
                  lline += 1

                  #Check that lline does not run to the end of the file
                  if(lline >= len(fl)):
                     print("The "+lab.lower()+" "+func_name+" in file "+fi+" line "+str(iline)+" was not terminated properly")
                     print("This means, check for additional spaces between "+lab.lower()+" and "+func_name+" and check if the name is given at the end again")
                     exit(-1)

                  if not fl[lline].upper().strip().startswith("!") and "INTENT(" in fl[lline].upper():
                        kline = max(kline,lline)
               lline += -1


               # store the data for the function and use it in the tex file
               # replace the ../ by nothing because of the latex funtion used does not do a replacement
               funcdoc_data.append([func_name,jline+1,iline+1,kline+1,lline+1,os.path.join(root.replace(rpath+"../",""),fi)])


# Include all dirs for precompiler flags and options
whitelist       = [rpath+"../cmake",rpath+"../src",rpath+"../doc",rpath+"../testing"]
for root, dirs, files in os.walk(rpath+"../",topdown=True):

   # Modify the directories such that only paths in whitelist are followed (no build paths in <root>)
   dirs[:] = [ d for d in dirs for w in whitelist if os.path.join(root,d).startswith(w) ]

   for fi in files:            

      # gather info from fortran files
      if fi.endswith(".F90"):
         fpath = os.path.join(root, fi)
         f = open(fpath,'r')
         fl = f.readlines()
         f.close()
         for iline,line in enumerate(fl):
            uline = line.upper().strip()
            line  = line.strip()
            # find all used precompiler flags
            if uline.startswith("#IFDEF"):
               precompiler_set.append(uline.strip().split()[1].upper())
      # gather info from cmake files
      if fi.endswith(".cmake") or fi == "CMakeLists.txt" :
         fpath = os.path.join(root, fi)
         f = open(fpath,'r')
         for line in f:
            if line.upper().strip().startswith("OPTION("):
               cmake_opts_set.append(line.upper().strip().replace("OPTION(","").split()[0])

precompiler_set = set(precompiler_set)
cmake_opts_set  = set(cmake_opts_set)
paramco_set     = set(paramco_set)


# read the lines of the file
ifile = open(rpath+"doc.tex",'r')
slins = ifile.readlines()
ifile.close()

# define output file
ofile = open(rpath+"doc_work.tex",'w')

# get the set of documented precompiler variables
doc_precomp = []
doc_cmakeop = []
doc_paramco = []

# search the source document doc.tex for "%UPDATE_" and a keyword, then update the lslisting afterwards
ilin = 0
while ilin < len(slins):

   #get the line
   line = slins[ilin]

   # if an update is found, perform the update and write the result, also increment ilin respectively
   if line.strip().startswith("%UPDATE_"):


      #analyze
      second_part =line[8:].strip()

      if(second_part == "OLD_TENSOR_TYPE"):
         #write the functional comment
         ofile.write(line)
         ilin = update_old_tensor_type(ofile,slins,ilin,rpath)
      elif(second_part == "MISSING_FUNCTIONS"):
         # build the new line with the updated numbers
         for ilist, nlist in enumerate(funcdoc_data):
            extra_line = "\interfacedto{"+nlist[0]+"}{generated}{"+str(nlist[2])+"}{"+str(nlist[4])+"}{"+nlist[5]+"}\n"
            ofile.write(extra_line)

         del funcdoc_data
         #write the functional comment
         ofile.write(line)
      else:
         print("Update command not found: "+second_part+" of line: "+str(ilin))
         exit(-1)

   # check precompiler flags
   elif line.strip().startswith("\PCFlag{"):
      b_var = line.find("\PCFlag{")+len("\PCFlag{")
      e_var = line.find("}",b_var)
      if( e_var == -1 ):
         print("My OCD is not pleased: \PCFlag{: parenthesis not closed in line "+str(ilin))
         exit(-1)
      doc_precomp.append( line[ b_var : e_var ] )
      ofile.write(line)

   # check cmake options
   elif "\cmakeoption{" in line:
      b_var = line.find("\cmakeoption{")+len("\cmakeoption{")
      e_var = line.find("}",b_var)
      if( e_var == -1 ):
         print("My OCD is not pleased: cmakeoption: parenthesis not closed in line "+str(ilin))
         exit(-1)
      doc_cmakeop.append( line[ b_var : e_var ] )
      ofile.write(line)

   # check public parameters and counters
   elif line.strip().startswith("\paramcounter{"):
      b_var = line.find("\paramcounter{")+len("\paramcounter{")
      e_var = line.find("}",b_var)
      if( e_var == -1 ):
         print("My OCD is not pleased: cmakeoption: parenthesis not closed in line "+str(ilin))
         exit(-1)
      doc_paramco.append( line[ b_var : e_var ] )
      ofile.write(line)

   # check whether a function has been documented in the doc.tex
   elif line.strip().startswith("\subfuncint{"):
      b_var = line.find("\subfuncint{")+len("\subfuncint{")
      e_var = line.find("}",b_var)
      if( e_var == -1 ):
         print("My OCD is not pleased: cmakeoption: parenthesis not closed in line "+str(ilin))
         exit(-1)
      found_and_updated = False

      for ilist, nlist in enumerate(funcdoc_data):
         if line[ b_var : e_var ] == nlist[0] :
            ilin = update_subfuncint(ofile,slins,ilin,nlist,rpath)
            del funcdoc_data[ilist]
            found_and_updated = True
            break

   # check whether a function has been documented in the doc.tex
   elif line.strip().startswith("\interfacedto{"):
      b_var = line.find("\interfacedto{")+len("\interfacedto{")
      e_var = line.find("}",b_var)
      if( e_var == -1 ):
         print("My OCD is not pleased: cmakeoption: parenthesis not closed in line "+str(ilin))
         exit(-1)
      found_and_updated = False

      for ilist, nlist in enumerate(funcdoc_data):
         if line[ b_var : e_var ] == nlist[0] :
            ilin = update_interfacedto(ofile,slins,ilin,nlist,rpath)
            del funcdoc_data[ilist]
            found_and_updated = True
            break

      if not found_and_updated:
         print("My OCD is not pleased: could not find function, subroutine or interface ",line[ b_var : e_var ]," in any .F90 file, maybe it is referenced twice by \subfuncint?")
         exit(-1)

   # nothing do do, just write the unmodified line
   else:
      ofile.write(line)

   # update the counter
   ilin += 1

warn = docfound
if warn :
   print("WARNINGS DETECTED!!!")

# Sanity checks
ok = True
for defd in doc_precomp:
   if defd not in precompiler_set:
      ok = False
      print("My OCD is not pleased: precompiler var "+defd+" is documented but not used")
      exit(-1)
for defd in precompiler_set:
   if defd not in doc_precomp:
      ok = False
      print(defd)
      print("My OCD is not pleased: precompiler var "+defd+" is used but not documented")
      exit(-1)
for defd in doc_cmakeop:
   if defd not in cmake_opts_set:
      ok = False
      print("My OCD is not pleased: cmake option "+defd+" is documented but not used")
      exit(-1)
for defd in cmake_opts_set:
   if defd not in doc_cmakeop:
      ok = False
      print(defd)
      print("My OCD is not pleased: cmake option "+defd+" is used but not documented")
      exit(-1)

#for nlist in funcdoc_data:
#   print "My OCD is not pleased: function "+nlist[0]+" is not referred to in doc.tex"

if( ok ):
   # normal termination
   move(rpath+"doc_work.tex",rpath+"doc.tex")
   print("TESTSTATUS: GOOD")
   exit(0)
else:
   # errors before termination
   print("ERRORS DETECTED")
   exit(-1)
