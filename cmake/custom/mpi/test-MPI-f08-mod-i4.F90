program test_mpi

   use mpi_f08

   implicit none
   integer(kind=4)  :: ierr
   type(MPI_Comm)   :: comm
   type(MPI_Status) :: stat
   integer(kind=mpi_address_kind) :: lb, extent
   integer(kind=4) :: s = MPI_ANY_SOURCE
   integer(kind=4) :: t = MPI_ANY_TAG

   call mpi_init(ierr)
   call mpi_type_get_extent(mpi_double_precision,lb,extent,ierr)
   call mpi_probe(s, t, comm, stat, ierr)
   call mpi_finalize(ierr)

end program test_mpi
