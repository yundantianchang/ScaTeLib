program test_mpi
   implicit none
   include "mpif.h"
   integer :: ierr
   integer(kind=mpi_address_kind) :: lb, extent

   call mpi_init(ierr)
   call mpi_type_get_extent(mpi_double_precision,lb,extent,ierr)
   call mpi_finalize(ierr)

end program test_mpi
