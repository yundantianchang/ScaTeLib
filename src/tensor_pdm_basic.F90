!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Author:   Patrick Ettenhuber (pettenhuber@gmail.com)
! Date:     April, 2013
! File:     This file contains the basic funcitonality for pdm operations
! License : This file is subject to all the licensing conditions under which
!           ScaTeLib is distributed
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module tensor_pdm_basic_module

  use get_idx_mod
  use tensor_parameters_module
  use tensor_counters_module
  use tensor_allocator_module
  use tensor_bg_buf_module
  use tensor_type_def_module
  use tensor_mpi_operations_module
#ifdef VAR_MPI
  use tensor_mpi_interface_module
#endif
  use tensor_error_handler
  use tensor_tile_type_module
  use reorder_frontend_module

  public :: tensor_set_comm
  private

  contains

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   late 2015
  !Intent: Set the communicator for the tensors
  !Input:
  !       -comm: the communicator
  subroutine tensor_set_comm(comm)
     implicit none
     integer(tensor_mpi_kind), target, intent(in) :: comm
     tensor_work_comm => comm
  end subroutine tensor_set_comm

end module tensor_pdm_basic_module
