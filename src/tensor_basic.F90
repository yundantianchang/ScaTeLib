!> @file
!> basic tensor functionality
module tensor_basic_module
  use,intrinsic :: iso_c_binding, only:c_loc,c_f_pointer,c_null_ptr

  use tensor_error_handler
  use tensor_parameters_module
  use tensor_counters_module
  use tensor_allocator_module
  use tensor_tile_type_module
  use tensor_pdm_basic_module
  use tensor_type_def_module

!  interface tensor_set_tdims
!     module procedure tensor_set_tdims4,tensor_set_tdims8
!  end interface tensor_set_tdims

!  public :: tensor_set_tdims
!  public :: memory_allocate_tensor_dense
!  public :: memory_deallocate_tensor_dense
!  public :: tensor_reset_value_defaults
!  public :: tensor_free_basic
!  public :: tensor_init_lock_set
!  public :: tensor_nullify_pointers
  public :: tensor_print_memory_currents
!  public :: tensor_set_addr
!  public :: tensor_set_ntpm
  private

  contains

    !> \brief to accurately account for mem on each node
    ! count mem allocated in each of the arrays many pointers
    !> \author Patrick Ettenhuber
!    subroutine tensor_set_tdims4(arr,tdims,mode)
!      implicit none
!      type(tensor),intent(inout) :: arr
!      integer(tensor_standard_int),intent(in) :: tdims(arr%mode)
!      integer, optional :: mode
!      include "tensor_set_tdims.inc"
!    end subroutine tensor_set_tdims4
!    subroutine tensor_set_tdims8(arr,tdims,mode)
!      implicit none
!      type(tensor),intent(inout) :: arr
!      integer(tensor_long_int),intent(in) :: tdims(arr%mode)
!      integer, optional :: mode
!      include "tensor_set_tdims.inc"
!    end subroutine tensor_set_tdims8
!  
!    subroutine tensor_init_lock_set(arr)
!      implicit none
!      type(tensor),intent(inout) :: arr
!      integer(tensor_long_int) :: vector_size
!
!      if(.not.associated(arr%lock_set))then
!        call tensor_alloc_mem(arr%lock_set,arr%ntiles)
!        !$OMP CRITICAL
!        vector_size = int(size(arr%lock_set)*tensor_standard_log,kind=tensor_long_int)
!        tensor_counter_aux_a_mem = tensor_counter_aux_a_mem + vector_size
!        tensor_counter_memory_in_use  = tensor_counter_memory_in_use  + vector_size
!        !$OMP END CRITICAL
!        arr%lock_set = .false.
!      endif
!      
!    end subroutine tensor_init_lock_set
!
!    !> \brief to accurately account for mem on each node
!    ! count mem allocated in each of the arrays many pointers
!    !> \author Patrick Ettenhuber
!    subroutine tensor_set_ntpm(arr,ntpm,mode)
!       implicit none
!       type(tensor),intent(inout) :: arr
!       integer(tensor_standard_int),intent(in) :: ntpm(arr%mode)
!       integer, optional :: mode
!       integer(tensor_long_int) :: vector_size
!       integer :: i
!       if (present(mode))then
!          if((arr%mode/=0 .and. arr%mode/=mode).or.arr%mode==0)then
!             print *,"mode",mode,"arr%mode",arr%mode      
!             call tensor_status_quit("wrong use of tensor_set_ntpm",lspdm_errout)
!          else
!             arr%mode=mode
!          endif
!       endif
!       vector_size = int(arr%mode*tensor_standard_int,kind=tensor_long_int)
!       if (associated(arr%ntpm))then
!          !$OMP CRITICAL
!          tensor_counter_aux_f_mem = tensor_counter_aux_f_mem + vector_size
!          tensor_counter_memory_in_use    = tensor_counter_memory_in_use  - vector_size
!          !$OMP END CRITICAL
!          call tensor_free_mem(arr%ntpm)
!       endif
!       if(.not.associated(arr%ntpm))then
!          call tensor_alloc_mem(arr%ntpm,arr%mode)
!          !$OMP CRITICAL
!          tensor_counter_aux_a_mem = tensor_counter_aux_a_mem + vector_size
!          tensor_counter_memory_in_use  = tensor_counter_memory_in_use  + vector_size
!          !$OMP END CRITICAL
!       endif
!       do i=1,arr%mode
!          arr%ntpm(i)=ntpm(i)
!       enddo
!    end subroutine tensor_set_ntpm
!
!    subroutine tensor_set_addr(arr,addr,nnodes)
!      implicit none
!      type(tensor),intent(inout) :: arr
!      integer,intent(in) :: addr(*)
!      integer(tensor_mpi_kind) :: nnodes
!      integer(tensor_standard_int) :: vector_size
!      integer :: i
!
!      vector_size = int(nnodes*tensor_standard_int,kind=tensor_long_int)
!      if (associated(arr%addr_p_arr))then
!         !$OMP CRITICAL
!         tensor_counter_aux_f_mem     = tensor_counter_aux_f_mem      + vector_size
!         tensor_counter_memory_in_use = tensor_counter_memory_in_use  - vector_size
!         !$OMP END CRITICAL
!         call tensor_free_mem(arr%addr_p_arr)
!      endif
!      if(.not.associated(arr%addr_p_arr))then
!         call tensor_alloc_mem(arr%addr_p_arr,nnodes)
!         !$OMP CRITICAL
!         tensor_counter_aux_a_mem     = tensor_counter_aux_a_mem     + vector_size
!         tensor_counter_memory_in_use = tensor_counter_memory_in_use + vector_size
!         !$OMP END CRITICAL
!      endif
!      do i=1,nnodes
!         arr%addr_p_arr(i)=addr(i)
!      enddo
!    end subroutine tensor_set_addr
!
!
!  !> \brief Allocate memory for general arrays with memory statistics
!  !> \author Patrick Ettenhuber adapted from Marcin Ziolkowski
!    subroutine memory_allocate_tensor_dense(arr,bg)
!      implicit none
!      type(tensor),intent(inout) :: arr
!      logical, intent(in) :: bg
!      integer(tensor_long_int) :: vector_size
!      real(tensor_dp) :: tcpu1,twall1,tcpu2,twall2
!      integer(8) :: ne
!      logical :: loc,parent
!
!      !call memory_deallocate_array(arr)
!      if(associated(arr%elm1)) then
!        call tensor_status_quit("ERROR(memory_allocate_array):array already initialized, please free first",lspdm_errout)
!      endif
!      vector_size = int((arr%nelms)*tensor_dp,kind=tensor_long_int)
!
!      call tensor_alloc_mem(arr%elm1,arr%nelms,bg=bg)
!      arr%bg_alloc = bg
!
!      if( tensor_debug_mode )then
!         arr%elm1 = 0.0E0_tensor_dp
!      endif
!
!!$OMP CRITICAL
!      tensor_counter_dense_a_mem = tensor_counter_dense_a_mem + vector_size
!      tensor_counter_memory_in_use = tensor_counter_memory_in_use + vector_size
!      !tensor_counter_max_memory = max(tensor_counter_max_memory,tensor_counter_memory_in_use)
!!$OMP END CRITICAL
!      
!      call assoc_ptr_arr(arr)
!
!
!    end subroutine memory_allocate_tensor_dense
!
!  !> \brief deallocate dense part
!  !> \author Patrick Ettenhuber
!  !> \param array for which elm1 should be deallocated
!    subroutine memory_deallocate_tensor_dense(arr)
!      implicit none
!      type(tensor) :: arr
!      integer(tensor_long_int) :: vector_size
!      real(tensor_dp) :: dim1
!      real(tensor_dp) :: tcpu1,twall1,tcpu2,twall2
!      logical :: bg
!
!
!      if(associated(arr%elm1)) then
!
!         vector_size = int(size(arr%elm1)*tensor_dp,kind=tensor_long_int)
!
!         bg=arr%bg_alloc
!         call tensor_free_mem(arr%elm1,bg=bg)
!
!         !endif
!         call deassoc_ptr_arr(arr)
!
!!$OMP CRITICAL
!         tensor_counter_dense_f_mem   = tensor_counter_dense_f_mem   + vector_size
!         tensor_counter_memory_in_use = tensor_counter_memory_in_use - vector_size
!!$OMP END CRITICAL
!      end if
!
!
!
!    end subroutine memory_deallocate_tensor_dense
!
!
!
!  !> \brief deallocate tiles and keep track of memory
!  !> \author Patrick Ettenhuber
!  !> \date September 2012
!    subroutine memory_deallocate_tile(arr)
!      implicit none
!      type(tensor),intent(inout) :: arr
!      integer(tensor_long_int) :: vector_size
!      real(tensor_dp) :: dim1,dim2,dim3,dim4
!      real(tensor_dp) :: tcpu1,twall1,tcpu2,twall2
!      integer :: i
!      logical :: bg
!
!
!      do i=arr%nlti,1,-1
!        if(associated(arr%ti(i)%t)) then
! 
!           if( .not. alloc_in_dummy )then
!
!              vector_size = int(size(arr%ti(i)%t)*tensor_dp,kind=tensor_long_int)
!
!              bg = arr%bg_alloc
!#ifdef VAR_MPI
!              call tensor_free_mem(arr%ti(i)%t,arr%ti(i)%c,bg=bg)
!#else
!              call tensor_free_mem(arr%ti(i)%t,bg=bg)
!#endif
!
!              !$OMP CRITICAL
!              tensor_counter_tiled_f_mem   = tensor_counter_tiled_f_mem   + vector_size
!              tensor_counter_memory_in_use = tensor_counter_memory_in_use - vector_size
!              !$OMP END CRITICAL
!           endif
!
!           arr%ti(i)%t => null()
!
!           vector_size = int(size(arr%ti(i)%d)*tensor_int,kind=tensor_long_int)
!           call tensor_free_mem(arr%ti(i)%d)
!           !$OMP CRITICAL
!           tensor_counter_aux_f_mem     = tensor_counter_aux_f_mem     + vector_size
!           tensor_counter_memory_in_use = tensor_counter_memory_in_use - vector_size
!           !$OMP END CRITICAL
!        end if
!
!      enddo
!
!      vector_size = int(arr%nlti*tensor_bytes_per_tile,kind=tensor_long_int)
!!$OMP CRITICAL
!      tensor_counter_aux_f_mem     = tensor_counter_aux_f_mem     + vector_size
!      tensor_counter_memory_in_use = tensor_counter_memory_in_use - vector_size
!!$OMP END CRITICAL
!
!      call tensor_free_mem(arr%ti)
!
!
!
!    end subroutine memory_deallocate_tile

  !> \brief Print statistics of array4 objects
  !> \author Patrick Ettenhuber
  !> \param output File unit for output 
    subroutine print_memory_statistics(output)

      implicit none
      integer, intent(in) :: output

      write(lspdm_stdout,'(/,a)')    '  Array memory statistics    '
      write(lspdm_stdout,'(a)')      ' =================================================='
      write(lspdm_stdout,'(a,f12.2,a)') ' Allocated memory    : ',tensor_counter_dense_a_mem/2**20,' MB'
      write(lspdm_stdout,'(a,f12.2,a)') ' Deallocated memory  : ',tensor_counter_dense_f_mem/2**20,' MB'
      write(lspdm_stdout,'(a,f12.2,a)') ' Alloc-dealloc mem   : ', &
           (tensor_counter_dense_a_mem-tensor_counter_dense_f_mem)/2**20,' MB'
      write(lspdm_stdout,'(a,f12.2,a)') ' Memory in use       : ',tensor_get_total_mem()/2**20,' MB'
      write(lspdm_stdout,'(a,f12.2,a)') ' Max memory in use   : ',tensor_counter_max_hp_mem/2**20,' MB'

      write(lspdm_stdout,'(a,/,a)') '  Time ', &
           ' ======='

    end subroutine print_memory_statistics
  
  !> \brief Print currenly used memory and max allocated memory so far
  !> \author Marcin Ziolkowski, modified by Patrick Ettenhuber
  !> \param output File unit for output
  subroutine tensor_print_memory_currents(output,retour)
    implicit none
    !> selects the output
    integer, intent(in) :: output
    !> selects to redcue it on master for checking, or outut instead
    integer(tensor_long_int), parameter :: nmeminfo = 9
    integer(tensor_long_int),intent(inout),optional :: retour(nmeminfo)
    integer(tensor_long_int) :: ret(nmeminfo)

    ret(1)=tensor_counter_dense_a_mem
    ret(2)=tensor_counter_dense_f_mem
    ret(3)=tensor_counter_tiled_a_mem
    ret(4)=tensor_counter_tiled_f_mem
    ret(5)=tensor_counter_aux_a_mem
    ret(6)=tensor_counter_aux_f_mem
    ret(7)=tensor_counter_memory_in_use_heap
    ret(8)=tensor_counter_max_hp_mem
    ret(9)=tensor_counter_max_bg_mem
    
    if(.not.present(retour))then
      write(*,'(a,i12,a)') ' Allocated memory for dense array   :',ret(1),' bytes'
      write(*,'(a,i12,a)') ' Deallocated memory for dense array :',ret(2),' bytes'
      write(*,'(a,i12,a)') ' Allocated memory for tiled array   :',ret(3),' bytes'
      write(*,'(a,i12,a)') ' Deallocated memory for tiled array :',ret(4),' bytes'
      write(*,'(a,i12,a)') ' Allocated aux memory for array     :',ret(5),' bytes'
      write(*,'(a,i12,a)') ' Dellocated aux memory for array    :',ret(6),' bytes'
      write(*,'(a,i12,a)') ' Memory in use for array            :',ret(7),' bytes'
      write(*,'(a,i12,a)') ' Max heap memory in use for array   :',ret(8),' bytes'
      write(*,'(a,i12,a)') ' Max bg memory in use for array     :',ret(9),' bytes'
    else
       retour = ret
    endif

  end subroutine tensor_print_memory_currents

  
!  !> \author Patrick Ettenhuber 
!  !> \date September 2012
!  !> \brief free the array structure
!  subroutine tensor_free_basic(arr)
!    implicit none
!    type(tensor), intent(inout) :: arr
!    if(associated(arr%elm1))call memory_deallocate_tensor_dense(arr)
!    if(associated(arr%ti))  call memory_deallocate_tile(arr)
!    call tensor_free_aux(arr)
!    if(associated(arr%access_type)) deallocate( arr%access_type )
!    if(associated(arr%label))       deallocate( arr%label )
!  end subroutine tensor_free_basic
!
!  !> \author Patrick Ettenhuber
!  !> \date October 2014
!  !> \brief set all values to invalid
!  subroutine tensor_reset_value_defaults(arr)
!     implicit none
!     type(tensor) :: arr
!     arr%local_addr  = -1
!     arr%ntiles      = -1
!     arr%nlti        = -1
!     arr%tsize       = -1
!     arr%offset      = -1
!     arr%zeros       = .false.
!     arr%initialized = .false.
!     arr%nnod        = -1
!     arr%comm        = -1
!  end subroutine tensor_reset_value_defaults
!
!  !> \author Patrick Ettenhuber
!  !> \date September 2012
!  !> \brief nullify all pointers in the array
!  subroutine tensor_nullify_pointers(arr)
!    implicit none
!    type(tensor), intent(inout) :: arr
!    NULLIFY(arr%dims)     
!    NULLIFY(arr%elm1)     
!    NULLIFY(arr%elm2)     
!    NULLIFY(arr%elm3)     
!    NULLIFY(arr%elm4)     
!    NULLIFY(arr%elm5)     
!    NULLIFY(arr%elm6)     
!    NULLIFY(arr%elm7)     
!    NULLIFY(arr%dummy)    
!    NULLIFY(arr%ti)       
!    NULLIFY(arr%wi)       
!    NULLIFY(arr%ntpm)     
!    NULLIFY(arr%tdim)     
!    NULLIFY(arr%addr_p_arr)
!    NULLIFY(arr%access_type)
!    NULLIFY(arr%label)
!    arr%dummyc = c_null_ptr
!  end subroutine tensor_nullify_pointers

end module  tensor_basic_module
