!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!
! Author:   Patrick Ettenhuber (pettenhuber@gmail.com)
! Date:     June, 2015
! File:     This file contains the blas1 like PDM tensor operations
! License : This file is subject to all the licensing conditions under which
!           ScaTeLib is distributed
!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
module tensor_blas1_like_module
use,intrinsic :: iso_c_binding,only:c_f_pointer,c_loc

use get_idx_mod
use tensor_parameters_module
use tensor_counters_module
use tensor_mpi_binding_module
use tensor_bg_buf_module
use tensor_buffer_handling_module
use tensor_mpi_interface_module
use tensor_mpi_operations_module
use tensor_type_allocator_module
use tensor_type_def_module

use tensor_basic_module
use tensor_pdm_basic_module
use tensor_pdm_jobs_module

use tensor_DenseTensor_type_module
use tensor_TiledTensor_type_module
use tensor_pdm_operations_simple_module

#ifdef VAR_MPI
  use TiledDistributedTensor_acc_tile_module, only: TiledDistributedTensor_acc_tile
  use TiledDistributedTensor_put_tile_module, only: TiledDistributedTensor_put_tile
  use TiledDistributedTensor_get_tile_module, only: TiledDistributedTensor_get_tile
#endif

#ifdef VAR_MPI
public :: PDMTensor_add_par
public :: PDMTensor_dot_par
public :: PDMTensor_hmul_par
public :: PDMTensor_cp
#endif
private
contains

#ifdef VAR_MPI
  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   October 2015
  !Intent: calcualte the dot product of two tensors
  !Input:
  !       -A: source tensor 1
  !       -B: source tensor 2
  !       -o: B-[order]->A
  !       -buf: buffer specification
  !       -force_sync: synchronize the nodes after the operation
  !Output:
  !       -res: the dot product
  function PDMTensor_dot_par(A,B,dest,o,buf,force_sync) result(res)
     implicit none
     class(TiledDistributedTensor), intent(in) :: A
     class(SweetTensor), intent(in)            :: B
     integer, intent(in)                       :: dest
     integer, intent(in)                       :: o(B%mode)
     type(buffer_spec), intent(in), optional   :: buf
     logical, intent(in), optional                :: force_sync
     !internal variables
     real(tensor_dp) :: pre1, pre2
     integer, parameter       :: JOB        = JOB_TENSOR_DDOT 
     integer, parameter       :: JOB_BDENSE = JOB_TENSOR_DDOT_BDENSE
     character(17), parameter :: srname     = "PDMTensor_dot_par"
     !external BLAS
     real(tensor_dp), external     :: ddot
     include "tensor_blas1_pre.inc"

        res = res + ddot(nelmsTA,wA,1,wB,1)

     include "tensor_blas1_post.inc"


     call tensor_mpi_allreduce(res,tensor_work_comm)

  end function PDMTensor_dot_par

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   October 2015
  !Intent: add two tensors
  !Input:
  !       -pre1: scaling factor for the operation
  !       -B: source tensor
  !       -o: B-[order]->A
  !       -buf: buffer specification
  !       -force_sync: synchronize the nodes after the operation
  !Output:
  !       -A: destination tensor
  subroutine PDMTensor_add_par(pre1,A,pre2,B,o,buf,force_sync)
     implicit none
     class(TiledDistributedTensor), intent(inout) :: A
     class(SweetTensor), intent(in)               :: B
     integer, intent(in)                          :: o(B%mode)
     type(buffer_spec), intent(in), optional      :: buf
     real(tensor_dp) :: pre1, pre2
     logical, intent(in), optional                :: force_sync
     !internal variables
     integer, parameter       :: JOB        = JOB_ADD_PAR
     integer, parameter       :: JOB_BDENSE = JOB_ADD_PAR_BDENSE
     character(17), parameter :: srname     = "PDMTensor_add_par"
     include "tensor_blas1_pre.inc"

        if( abs(pre1 - 0.0E0_tensor_dp) < 1.0E-13_tensor_dp) wA = 0.0E0_tensor_dp
        if( abs(pre1 - 1.0E0_tensor_dp) > 1.0E-13_tensor_dp) call dscal(nelmsTA,pre1,wA,1)
        call daxpy(nelmsTA,pre2,wB,1,wA,1)

     include "tensor_blas1_post.inc"

  end subroutine PDMTensor_add_par

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   October 2015
  !Intent: calculate the hadamard product of two tensors
  !Input:
  !       -pre1: scaling factor for the operation
  !       -B: source tensor
  !       -o: B-[order]->A
  !       -buf: buffer specification
  !       -force_sync: synchronize the nodes after the operation
  !Output:
  !       -A: destination tensor
  subroutine PDMTensor_hmul_par(pre1,A,B,o,buf,force_sync)
     implicit none
     class(TiledDistributedTensor), intent(inout) :: A
     class(SweetTensor), intent(in)               :: B
     integer, intent(in)                          :: o(B%mode)
     type(buffer_spec), intent(in), optional      :: buf
     real(tensor_dp) :: pre1, pre2
     logical, intent(in), optional                :: force_sync
     !internal variables
     integer, parameter       :: JOB        = JOB_HMUL_PAR
     integer, parameter       :: JOB_BDENSE = JOB_HMUL_PAR_BDENSE
     character(18), parameter :: srname     = "PDMTensor_hmul_par"
     include "tensor_blas1_pre.inc"

        !$omp parallel do default(none) private(i) shared(nelmsTA,wA,wB,pre1)
        do i = 1,nelmsTA
           wA(i) = pre1*wA(i)*wB(i)
        enddo
        !$omp end parallel do

     include "tensor_blas1_post.inc"

  end subroutine PDMTensor_hmul_par

  !Author: Patrick Ettenhuber (pettenhuber@gmail.com)
  !Date:   October 2015
  !Intent: cp PDM tensor data
  !Input:
  !       -B: source tensor
  !       -o: B-[order]->A
  !       -buf: buffer specification
  !       -force_sync: synchronize the nodes after the operation
  !Output:
  !       -A: destination tensor
  subroutine PDMTensor_cp(A,B,o,buf,force_sync)
     implicit none
     class(TiledDistributedTensor), intent(inout) :: A
     class(SweetTensor), intent(in)               :: B
     integer, intent(in)                          :: o(B%mode)
     type(buffer_spec), intent(in), optional      :: buf
     real(tensor_dp) :: pre1, pre2
     logical, intent(in), optional                :: force_sync
     !internal variables
     integer, parameter       :: JOB        = JOB_CP_ARR
     integer, parameter       :: JOB_BDENSE = JOB_CP_ARR_BDENSE
     character(12), parameter :: srname     = "PDMTensor_cp"
     include "tensor_blas1_pre.inc"

        !omp workshare
        !wA = wB
        call dcopy(nelmsTA,wB,1,wA,1)
        !omp end workshare

     include "tensor_blas1_post.inc"

  end subroutine PDMTensor_cp
#else
  ! Dummy routine to avoid warnings
  subroutine tensor_blas1_like_module_dummy()
     implicit none
  end subroutine tensor_blas1_like_module_dummy
#endif
end module tensor_blas1_like_module
